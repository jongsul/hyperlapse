/*start test code*/
var FileManage = {
	/*  saveRoute : function(hObj){
	    console.log('saveRoute');
	    console.log(JSON.stringify(hObj));
	    return 3;
	  },*/

		saveRoute : function(hObj, cb) {
			console.log('saveRoute');

			var fileId = Number(new Date());

			var documentsDir;
			function onerror(error) {
				console
						.log("The error "
								+ error.message
								+ " occurred when listing the files in the selected folder");
			}

			function onsuccess(files) {
				/*
				console.log("filesystem.resolve on success");
				var fileName = null;
				var arr = (hObj.summary).split("/");
				fileName = arr[0] + '.json';
				orgFileName = arr[0];
				console.log(fileName);
				var is_exist = false;
				var file_cnt = 0;

				var i = 0;
				for (; i < files.length; i++) {
					console.log("File Name " + i + " is " + files[i].name); // displays file name
					if (files[i].name == fileName) {
						console.log("fileName duplicate");
						file_cnt++;
						fileName = orgFileName + "(" + file_cnt + ").json";
						console.log(fileName);
						i = -1;
					}
				}

				var testFile = null;

				if (is_exist == false) {
					testFile = documentsDir.createFile(fileName);
					console.log("file create success");
				} else {
					console.log("filename error");
				}
				*/
				testFile = documentsDir.createFile(fileId);

				if (testFile != null) {
					testFile.openStream("w", function(fs) {
						hObj.fileId = fileId;
						console.log(hObj.status);
						fs.write(JSON.stringify(hObj));
						fs.close();
						cb(fileId);
						console.log("save finish");
						console.log("close file");
					}, function(e) {
						console.log("Error " + e.message);
					}, "UTF-8");
					
				}
				
			}

			tizen.filesystem.resolve('documents', function(dir) {
				documentsDir = dir;
				dir.listFiles(onsuccess, onerror);
				console.log("filesystem.resolve");
			}, function(e) {
				console.log("Error" + e.message);
			}, "rw");
		},
		
		updateRoute : function(fileId, recode, cb) {
			console.log('updateRoute');

			function onerror(error) {
				console
						.log("The error "
								+ error.message
								+ " occurred when listing the files in the selected folder");
			}

			function onsuccess(files) {
				var idx = 0;
				for (var i=0; i < files.length; i++) {
					if (files[i].isDirectory == false && files[i].name == fileId) {	//파일인 경우만 실행
						idx = i;
						break;
					}
				}
				
				files[idx].readAsText(function(str) {	//파일의 내용 읽기
					var obj = JSON.parse(str);	//파일 내용을 오브젝트로 변환
					files[idx].openStream("w", function(fs) {
						obj.recode = recode;
						fs.write(JSON.stringify(obj));
						fs.close();
						cb(fileId);
					}, function(e) {
						console.log("Error " + e.message);
					}, "UTF-8");
				}, function(e) {
					console.log("Error " + e.message);
				}, "UTF-8");
				
			}
			
			var documentsDir;
			tizen.filesystem.resolve('documents', function(dir) {
				documentsDir = dir;
				dir.listFiles(onsuccess, onerror);
				console.log("filesystem.resolve");
			}, function(e) {
				console.log("Error" + e.message);
			}, "rw");
		},

	getList : function(cb) {
		console.log('filegetList');
		var fileList = [];
		function onsuccess(files) {
			if (files.length == 0) {
				cb(fileList);
			}
			for ( var i = 0; i < files.length; i++) {
				console.log("File Name is " + files[i].name); // displays file name
				if (files[i].isDirectory == false) {
					files[i].readAsText(function(str) {
						var obj = JSON.parse(str);
						fileList.push(obj);
						if (fileList.length == files.length) {
							fileList.sort(function(obj1,obj2){return obj2.fileId-obj1.fileId});
							cb(fileList);
							console.log("*" + i + fileList);
						}

					}, function(e) {
						console.log("Error " + e.message);
					}, "UTF-8");
				}
			}
		}

		function onerror(error) {
			console
					.log("The error "
							+ error.message
							+ " occurred when listing the files in the selected folder");
		}

		var documentsDir;
		tizen.filesystem.resolve('documents', function(dir) {
			documentsDir = dir;
			dir.listFiles(onsuccess, onerror);
		}, function(e) {
			console.log("Error" + e.message);
		}, "rw");
	},

	getRoute : function(fileId, cb) {
		function onsuccess(files) {
			for ( var i = 0; i < files.length; i++) {
				if (files[i].isDirectory == false) {
					files[i].readAsText(function(str) {
						var obj = JSON.parse(str);
						if (obj.fileId == fileId) {
							cb(obj);
						}
					}, function(e) {
						console.log("Error " + e.message);
					}, "UTF-8");
				}
			}
		}

		function onerror(error) {
			console
					.log("The error "
							+ error.message
							+ " occurred when listing the files in the selected folder");
		}

		var documentsDir;
		tizen.filesystem.resolve('documents', function(dir) {
			documentsDir = dir;
			dir.listFiles(onsuccess, onerror);
		}, function(e) {
			console.log("Error" + e.message);
		}, "rw");
	},

	deleteRoute : function(fileId, cb) {
		console.log('deletion loaded');
		function onsuccess(files) {
			for ( var i = 0; i < files.length; i++) {
				if (files[i].isDirectory == false) {
					
					
					if (files[i].name == fileId) {
						console.log(i+"  "+files[i].name+" "+fileId);
						documentsDir.deleteFile(files[i].fullPath,
								function() {
									console.log("File Deleted");
									cb();
								}, function(e) {
									console.log("Error" + e.message);
								});
					}
					
				}
			}
		}

		function onerror(error) {
			console
					.log("The error "
							+ error.message
							+ " occurred when listing the files in the selected folder");
		}

		var documentsDir;
		tizen.filesystem.resolve('documents', function(dir) {
			documentsDir = dir;
			dir.listFiles(onsuccess, onerror);
		}, function(e) {
			console.log("Error" + e.message);
		}, "rw");

	}
}

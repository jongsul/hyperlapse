/**
 *   TEST CODE : 웹소켓 에코서버에 주기적으로 신호 전송 (아두이노 대신)
 */

var testcount = 0;
var test = function() {
	setInterval(function() {
			testcount += Math.floor(Math.random() * 2);
			oSocket.send(testcount);
			console.log('signal sent:', testcount);
	}, 100);
};		


/**
 *	웹소켓 서버에서 신호가 오면
 *  주행속도, 거리 계산
 *  view 컨트롤이 주행영상을 표시하도록 함수 호출
 */

// var WEBSOCKET_ADDRESS = 'ws://echo.websocket.org';
var WEBSOCKET_ADDRESS = 'ws://192.168.0.2';

var oSocket;
if('WebSocket' in window)
{
	oSocket = new WebSocket(WEBSOCKET_ADDRESS);

	// 신호 오면 주행거리,속도 계산 및 arduino call
    oSocket.onmessage = function(e) {
    	
    	console.log('new data!', e.data);
    	
    	if(window['arduinoCallback']) {
    		    		
//    		console.log("data:", e.data);
	    	var new_cycle = Number(e.data);			// 지금까지 바퀴 총 회전 수
	    	var old_cycle = travel.queue.pop();		// 직전까지의 바퀴 회전 수

			// 주행거리, 속도 계산
	    		// 큐에 10개의 데이터가 꽉 찬다면 가장 오래된 데이터부터 삭제해야 함.
	    		if (travel.queue.length >= 9)
	    			travel.queue.shift();

	    		travel.queue.push(old_cycle);
	    		travel.queue.push(new_cycle);

	    		var diff = new_cycle - old_cycle;
	    		// 총 주행거리 계산
	    		travel.distance = new_cycle * travel.__ROUND_WHEEL;
	    		// 속력계산
	    		travel.speed = (travel.queue[travel.queue.length - 1] - travel.queue[0]) * travel.__ROUND_WHEEL * 3.6;	    		
	    	
//	    		console.log('distance:', travel.distance, 'speed:', travel.speed);
//	    	}
//	    	else
//	    	{
//	    		console.log('Error: Ignore invalid packet arrival.')
//	    	}
		    	
	    	// 아두이노 프로세스 함수 호출 (m, km, bool)
	    	if (travel.speed > 45)
	    		travel.speed = 45;
	    	arduinoCallback(diff * travel.__ROUND_WHEEL, travel.speed);
    	}
    };
        
    oSocket.onopen = function(e) {
            console.log('open. send start signal.');

			// 아두이노를 사용할 때 코드
			// oSocket.send("start");
			
			// 아두이노 없이 웹소켓 에코서버를 사용할 때의 코드
			// test();
	};

    oSocket.onclose = function(e) {
    		oSocket.send('end');
            console.log('close');
	};
};

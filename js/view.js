/*
	file에 저장될 내용
	 panoId만 있으면 onPanoramaLoad -> composePanorama 가능할 듯
	
	save 관련
	 - view 에서는 불러온 다음 그 객체의 recode부분만 수정,
			해당 객체 그대로 save에 넘김
	
	아두이노 정보를 받고 호출하는 부분
	 - 이벤트 등록되어 있으면 
	 window.arduinoCallback = function(){
		 
	 };
	 arduinoCallback(distance);
	 
});
 */

/*
 method	routeSelect()
 return(or result)	
 description	경로 선택 페이지가 열릴때 실행되는 메소드로 구글 지도를 로드.

 method	snapToRoad()
 return(or result)	
 description	구글 맵상에서맵 포인터를 움직였을때 길 위로 포인터를 옮겨준다.

 method	generate (fileIndex)
 return(or result)	
 description	사용자가 download 버튼을 클릭했을때hyperlapse의 generater를 실행한다.

 method	listRoute ()
 return(or result)	
 description	경로 목록 페이지가 열릴때 실행되는 메소드로 목록을 출력

 method	loadFileDetail ()
 return(or result)	
 description	경로의 상세정보를 화면에 보여줌

 method	render ()
 return(or result)	
 description	경로 재생 페이지가 열릴때 실행되는 메소드로 타이젠 블루투스 관련 초기화와 재생할 경로 데이터를 받아옴

 method	changeLookAt (event)
 return(or result)	
 description	경로재생UI의 조이스틱부분을 움질일때 마우스 이벤트를 매개변수로 받아 화면 시점을 움직인다. 
 */

var ViewManage = {
	loadData : {},
	is_load : false,
	is_generate : false,
	hObj : null,
	hObjInit : function() {
		var $pano = $('#pano');
		$pano.empty();
		// var hyperlapse = new Hyperlapse(pano, {
		this.hObj = new Hyperlapse($pano[0], {
			fov : 90,
			millis : 1000 * 60 * 60,
			width : window.innerWidth,
			height : window.innerHeight,
			zoom : 2,
			distance_between_points : 1,
			max_points : 10,
			// elevation: _elevation
			use_elevation : false,
			// lookat: lookat_point,
			use_lookat : false
		});
	},
	hObjGenerate : function() {
		var self = this;
		var points = GMap.getPins();
		GMap.getLoadPoint(points, function(response) {
			if (response) {
				var data = response.routes[0];
				var leg = data.legs[0];
				self.loadData = {
					fileId : 0,
					fileLength : 0,
					summary : data.summary,
					pathInfo : {
						distance : leg.distance,
						endAddress : leg.end_address,
						created : Common.getNowDate('y-m-d')
					},
					recode : {
						lastScene : 0,
						playTime : 0,
						distance : 0
					},
					route : [],
					status : 'loading'
				};

				self.hObj.generate({
					route : response
				});
				self.hObj.onRouteProgress = function(e) {
					// console.log('onRouteProgress');
					GMap.printMarker(e.point.location);
					self.loadData.fileLength++;
				};

				self.hObj.onRouteComplete = function(e) {
					console.log('onRouteComplete');
					self.is_generate = false;
					self.loadData.status = 'finish';
					self.loadData.route = self.hObj.getHObj();
					FileManage.saveRoute(self.loadData, function(fileId) {
						self.loadData.fileId = fileId;
						delete self.loadData.route;
						self.changeRoute('list');
					});
				};
			}
		});
	},
	changeRoute : function(type) {
		console.log('changeRoute' + type);
		var self = this;
		if(this.loadData.status=='loading'){
	         alert("다운로드 중입니다."); return false; 
	      }  
		
		self.playTimeInterval.clear();
		PanoramaCtrl.playInterval.clear();
		$('nav>div').removeClass('selected');
		$('.view_area').hide();

		$('.js_' + type + '_view').show();
		$('.js_nav_' + type).addClass('selected');
		window.arduinoCallback = null;

		self[type + 'ViewInit']();

		if (self.loadData.fileId) {
			FileManage.updateRoute(self.loadData.fileId, self.loadData.recode);
		}
		
		if(type == 'play') {
			travel.reset();
			oSocket.send("start");			
		}
		else {
			//oSocket.send("end");
			
		}
	},
	selectViewInit : function() {
		var self = this;
		var $select_view = $('.js_select_view');
		// map init points
		var start = {
			x : '40.782120000000006',
			y : '-73.96240999999998'
		};
		var end = {
			x : '40.78801',
			y : '-73.95695'
		};
		var points = {};
		points.start = new google.maps.LatLng(start.x, start.y);
		points.end = new google.maps.LatLng(end.x, end.y);

		// google map init
		GMap.init('map', points);

		// button event add
		$select_view.unbind('click').on('click', '.js_search_btn', function() {
			GMap.findAddress($select_view.find('input[name=address]').val());
		}).on('click', '.js_droppin_btn', function() {
			if(self.is_generate){
				alert("다운로드 중입니다.");
				return false;	
			}
			GMap.drop_pins();
		}).on('click', '.js_download_btn', function() {
			if (self.is_generate) {
				alert("다운로드 중입니다.");
				return false;
			}
			self.is_generate = true;
			self.hObjInit();
			self.hObjGenerate();
			// self.changeRoute('list');
		});
	},

	listViewInit : function() {
		var self = this;
		var $list_view = $('.js_list_view');
		FileManage.getList(self.printFileList);

		$list_view.unbind('click').on('click', '.js_file_list>li', function(e) {
			var self = $(e.currentTarget);
			if ($('.js_file_detail', self).css('display') == 'none') {
				$('.js_file_detail').hide();
				$('.js_file_detail', self).show();
			} else {
				$('.js_file_detail').hide();
			}
		}).on('click', '.js_del_btn', function(e) {
			Common.cancelBubble(e);
			var $target = $(e.currentTarget);
			var fileId = $target.attr('data-file-id');
			FileManage.deleteRoute(fileId, function() {
				FileManage.getList(self.printFileList);
			});
		}).on(
				'click',
				'.js_file_list>li [data-status].js_finish',
				function(e) {
					Common.cancelBubble(e);
					if (self.is_load) {
						alert("로드중입니다.");
						return false;
					}
					self.is_load = true;
					var $target = $(e.currentTarget);
					var fileId = $target.attr('data-file-id');
					if (fileId > 0) { // file id가 있으면 file load
						self.hObjInit();
						FileManage.getRoute(fileId, function(fileData) {
							console.log('getRoute');
							console.log(JSON.stringify(fileData));
							self.hObj.setHObj(fileData.route);
							delete fileData.route;
							self.loadData = fileData;
							self.hObj.load();
						});

						self.hObj.onLoadProgress = function(e) {
							var progressPercent = ((e.position + 1)
									/ self.loadData.fileLength * 100);
							$target.parent().find('.progress .bluebar').css(
									'width', progressPercent + '%')
						};
						self.hObj.onLoadComplete = function(e) {
							self.is_load = false;
							self.changeRoute('play');
						};
					} else { // file id없는 경우 fail
						return false;
					}
				}).on('click', '.js_search_btn', function() {
			FileManage.getList(self.printFileList);
			// self.printFileList();
		});
	},
	printFileList : function(fileList) {
		console.log('printFileList: ' + fileList.length);
		console.log(JSON.stringify(fileList));
		var $list_view = $('.js_list_view');

		var search = $list_view.find('input[name=filename]').val();
		$list_view.find('.js_file_list').empty();
		if (!fileList) {
			console.log('list not exist');
			return false;
		}
		for (var i = 0; i < fileList.length; i++) {
			if (search != ''
					&& fileList[i].summary.toLowerCase().indexOf(
							search.toLowerCase()) < 0) {
				continue;
			}
			var sample = $list_view.find('.main_wrap .sample').clone();
			$('[data-summary]', sample).text(fileList[i].summary);
			$('[data-distance]', sample).text(
					fileList[i].pathInfo.distance.text);

			if (fileList[i].status != 'finish') {
				$('[data-status]', sample).text('로딩').attr('data-file-id', 0);
				$('.js_del_btn', sample).attr('data-file-id', 0);
			} else {
				$('[data-status]', sample).text('완료').attr('data-file-id',
						fileList[i].fileId).addClass('js_finish');
				$('.js_del_btn', sample).attr('data-file-id',
						fileList[i].fileId);
			}
			$('[data-created]', sample).text(
					"생성일: " + fileList[i].pathInfo.created);
			$('[data-address]', sample).text(
					"도착지: " + fileList[i].pathInfo.endAddress);
			if (fileList[i].recode.playTime > 0) {
				$('[data-recode]', sample)
						.text(
								"기록: "
										+ ViewManage
												.timeToStr(fileList[i].recode.playTime));
			} else {
				$('[data-recode]', sample).hide();
			}
			$('.js_file_list').append(sample.html());
		}
	},

	playViewInit : function() {
		var self = this;
		var $play_view = $('.js_play_view');
		if (!self.loadData.summary) {
			self.changeRoute('list');
			return false;
		}
		;

		$play_view.find('[data-name]').text(self.loadData.summary);
		self.playTimeInterval.set(function() {
			self.loadData.recode.playTime++;
			$play_view.find('[data-time]').text(
					"주행시간: " + self.timeToStr(self.loadData.recode.playTime));
		});

		window.arduinoCallback = self.arduinoInputProcess.callback;

		PanoramaCtrl.init();
		self.hObj.next();
		self.hObj.play();		
		arduinoCallback(self.loadData.recode.distance, 0);
	},
	playTimeInterval : {
		interval : null,
		set : function(callback) {
			callback();
			this.clear();
			this.interval = setInterval(callback, 1000);
		},
		clear : function() {
			if (this.interval) {
				clearInterval(this.interval);
			}
		}
	},
	arduinoInputProcess : {
		accFov : 0,
		accMillis : 100,
		hIndex : 1,
		callback : function(distance, speed, notAruino) {
			var self = ViewManage.arduinoInputProcess;
			var $play_view = $('.js_play_view');
			
			if(_isFinished){
				ViewManage.changeRoute('select');
				_isFinished = false;
				oSocket.send('end');
			}
				

			if (!notAruino) {
				PanoramaCtrl.playInterval.changeMode('auto');
			}

			speed = parseInt(speed);
			totalDistance = parseInt(ViewManage.loadData.recode.distance) + distance;
			ViewManage.loadData.recode.distance = totalDistance;
			$play_view.find('[data-distance]').text("이동거리: " + ViewManage.distanceToStr(totalDistance));
			$play_view.find('[data-speed]').text(Common.padLeft(speed, 2, '0'));

			if (speed > 0) {
				ViewManage.hObj.millis = 1000 - (speed * 20);
			} else {
				ViewManage.hObj.millis = 1000 * 60 * 60;
			}

			// self.accFov += (speed/20);
			// if(currentFov < MIN_FOV) {
			// self.accFov = 0;
			// ViewManage.hObj.setFOV(MAX_FOV);
			// if(self.hIndex >= ViewManage.hObj.length()) {
			// alert('목적지에 도착하였습니다.');
			// ViewManage.changeRoute('list');
			// } else {
			// self.hIndex++;
			// ViewManage.hObj.next();
			// }
			// } else {
			// ViewManage.hObj.setFOV(MAX_FOV-self.accFov);
			// }
		}
	},

	distanceToStr : function(distance) {
		var printDistance = '';
		if (distance < 1000) {
			printDistance += distance + 'm';
		} else {
			printDistance += (distance / 1000).toFixed(1) + 'km';
		}
		return printDistance;
	},
	timeToStr : function(playTime) {
		var printTime = '';
		if (playTime > 3600) {
			printTime += parseInt(playTime / 3600) + '시간 ';
			playTime = playTime % 3600;
		}
		if (playTime > 60) {
			printTime += parseInt(playTime / 60) + '분 ';
			playTime = playTime % 60;
		}
		printTime += playTime + '초';
		return printTime;
	}
};

var PanoramaCtrl = {
	originPt : {
		x : 0,
		y : 0
	},
	onDownPt : {
		x : 0,
		y : 0
	},
	is_moving : false,
	playInterval : {
		interval : null,
		speed : 0,
		intervalTime : 100, // ms
		set : function(speed, intervalTime) {
			var self = this;
			self.speed = self.speed + (speed ? speed : 0);
			self.intervalTime = intervalTime ? intervalTime : self.intervalTime;
			self.clear();
			if (self.speed < 0) {
				return false;
			}
			if (window['arduinoCallback']) {
				this.interval = setInterval(function() {
					var distance = self.speed * (self.intervalTime / 1000);
					arduinoCallback(distance, self.speed, true);
				}, self.intervalTime);
			}
		},
		clear : function() {
			if (this.interval) {
				clearInterval(this.interval);
			}
		},
		changeMode : function(type) {
			console.log('changeMode');
			$playView = $('.js_play_view');
			this.speed = 0;
			arduinoCallback(0, this.speed, true);
			this.clear();
			if (type == 'auto') {
				$('.js_speed_up_btn', $playView).hide();
				$('.js_speed_down_btn', $playView).hide();
				$('.js_speed_mode_btn[data-type=passive]', $playView).show();
				$('.js_speed_mode_btn[data-type=auto]', $playView).hide();
			} else {
				$('.js_speed_up_btn', $playView).show();
				$('.js_speed_down_btn', $playView).show();
				$('.js_speed_mode_btn[data-type=passive]', $playView).hide();
				$('.js_speed_mode_btn[data-type=auto]', $playView).show();
			}
		}
	},
	init : function() {
		var self = this;
		console.log('PanoramaCtrl');
		$('.js_play_view').unbind('click').on('click', '.js_speed_up_btn',
				function() {
					self.playInterval.set(5);
				}).on('click', '.js_speed_down_btn', function() {
			self.playInterval.set(-5);
		}).on('click', '.js_speed_mode_btn', function(e) {
			self.playInterval.changeMode($(e.currentTarget).attr('data-type'));
		});
		var supportTouch = $.support.touch;
		var touchStartEvent = supportTouch ? "touchstart" : "mousedown";
		var touchStopEvent = supportTouch ? "touchend" : "mouseup";
		var touchMoveEvent = supportTouch ? "touchmove" : "mousemove";
		$('#pano').unbind(touchStartEvent).unbind(touchMoveEvent).unbind(
				touchStopEvent)
		/*
		 * .on(touchStartEvent, function(e){ console.log('mousedown');
		 * console.log(e.targetTouches[0].clientX); e.preventDefault();
		 * self.is_moving = true;
		 * 
		 * self.onDownPt.x = e.clientX; self.onDownPt.y = e.clientY;
		 * 
		 * self.originPt.x = ViewManage.hObj.position.x; self.originPt.y =
		 * ViewManage.hObj.position.y; }) .on(touchMoveEvent, function(e){
		 * console.log('mousemove'); e.preventDefault(); var f =
		 * ViewManage.hObj.fov() / 500; if (self.is_moving) { var dx = (
		 * self.onDownPt.x - e.clientX ) * f; var dy = ( e.clientY -
		 * self.onDownPt.y ) * f; ViewManage.hObj.position.x = self.originPt.x +
		 * dx; ViewManage.hObj.position.y = self.originPt.y + dy;
		 * 
		 * console.log(JSON.stringify(self.onDownPt))
		 * console.log(JSON.stringify(self.originPt))
		 * console.log(JSON.stringify(ViewManage.hObj.position)) } })
		 * .on(touchStopEvent, function(){ console.log('mouseup');
		 * self.is_moving = false; });
		 * 
		 */
		document.getElementById('pano').addEventListener(
				touchStartEvent,
				function(e) {
					// console.log('mousedown');
					e.preventDefault();
					self.is_moving = true;

					var clientX = e.clientX ? e.clientX
							: e.targetTouches[0].clientX;
					var clientY = e.clientY ? e.clientY
							: e.targetTouches[0].clientY;
					self.onDownPt.x = clientX;
					self.onDownPt.y = clientY;

					self.originPt.x = ViewManage.hObj.position.x;
					self.originPt.y = ViewManage.hObj.position.y;
				});
		document.getElementById('pano').addEventListener(
				touchMoveEvent,
				function(e) {
					// console.log('mousemove');
					e.preventDefault();
					var f = ViewManage.hObj.fov() / 500;
					if (self.is_moving) {
						var clientX = e.clientX ? e.clientX
								: e.targetTouches[0].clientX;
						var clientY = e.clientY ? e.clientY
								: e.targetTouches[0].clientY;
						var dx = (self.onDownPt.x - clientX) * f;
						var dy = (clientY - self.onDownPt.y) * f;
						ViewManage.hObj.position.x = self.originPt.x + dx;
						ViewManage.hObj.position.y = self.originPt.y + dy;
					}
				});
		document.getElementById('pano').addEventListener(touchStopEvent,
				function() {
					// console.log('mouseup');
					self.is_moving = false;
				});
	}
};

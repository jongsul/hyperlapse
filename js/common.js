var Common = {
  cancelBubble: function(e) {
    var evt = e ? e:window.event;
    if (evt.stopPropagation) evt.stopPropagation();
    if (evt.cancelBubble!=null) evt.cancelBubble = true;
  },
  getNowDate: function(format) {
    var date = new Date();
    var y = date.getFullYear();
    var m = this.padLeft(date.getMonth()+1, 2, '0');
    var d = this.padLeft(date.getDate(), 2, '0');
    var h = this.padLeft(date.getHours(), 2, '0');
    var i = this.padLeft(date.getMinutes(), 2, '0');
    var s = this.padLeft(date.getSeconds(), 2, '0');
    
    return format.replace('y', y).replace('m', m).replace('d', d).replace('h', h).replace('i', i).replace('s', s);
  },
  padLeft: function (num, n, padStr){
    return Array(n-String(num).length+1).join(padStr||'0')+num;
  }
}
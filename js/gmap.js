var GMap = {
  map: {},
  points: {},
  pins: {},
  directions_service: {},
  travelMode: null,
  geocoder: {},
  init: function(targetId, points){
    var self = this;
    self.directions_service = new google.maps.DirectionsService();
    self.travelMode = google.maps.TravelMode.BICYCLING;
    self.geocoder = new google.maps.Geocoder();
    self.points = points ? points : {};
    var directions_renderer, streetview_service;
    
    var mapOpt = { 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: self.points.start,
      zoom: 15
    };
    self.map = new google.maps.Map(document.getElementById(targetId), mapOpt);
    
    //street view overlay
    var overlay = new google.maps.StreetViewCoverageLayer();
    overlay.setMap(self.map);

    directions_renderer = new google.maps.DirectionsRenderer({draggable:false, markerOptions:{visible: false}});
    directions_renderer.setMap(self.map);
    directions_renderer.setOptions({preserveViewport:true});
    
    self.setPins();
  },
  getPoints: function(){
    return this.points;
  },
  getPins: function(){
    return this.pins;
  },
  setPins: function() {
    var self = this;
    self.pins.camera = new google.maps.Marker({
      position: self.points.start,
      map: self.map,
      visible: false
    });

    self.pins.start = new google.maps.Marker({
      position: self.points.start,
      draggable: true,
      map: self.map
    });
    google.maps.event.addListener (self.pins.start, 'dragend', function (event) {
      self.getLoadPoint(self.pins.start.getPosition(), function(result) {
        result = result.routes[0].overview_path[0];
        self.pins.start.setPosition(result);
        self.points.start = result;
        //changeHash();
      });
    });

    self.pins.end = new google.maps.Marker({
      position: self.points.end,
      draggable: true,
      map: self.map
    });
    google.maps.event.addListener (self.pins.end, 'dragend', function (event) {
      self.getLoadPoint(self.pins.end.getPosition(), function(result) {
        result = result.routes[0].overview_path[0];
        self.pins.end.setPosition(result);
        self.points.end = result;
        //changeHash();
      });
    });
  },
  getLoadPoint: function(points, callback) {
//console.log('getLoadPoint');
    var request = { 
      origin: points,
      destination: points,
      travelMode: this.travelMode
    };
    if(points.start) {
      request.origin = points.start.position;
      request.destination = points.end.position;
    } else {
      request.origin = request.destination = points;
    }

    this.directions_service.route(request, function(response, status) {
//console.log(response);
      if(status==google.maps.DirectionsStatus.OK) callback(response);
      else callback(null);
    });
  },
  printMarker: function(location){
    var self = this;
    new google.maps.Marker({
      position: location,
      draggable: false,
      icon: "images/dot_marker.png",
      map: self.map
    });
  },
  findAddress: function(address) {
    var self = this;
    self.geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        self.map.setCenter(results[0].geometry.location);
        self.drop_pins();
      } else {
        show( "Geocode was not successful for the following reason: " + status );
      }
    });
  },
  
  drop_pins: function(){
    var self = this;
    var bounds = self.map.getBounds();
    var top_left = bounds.getNorthEast();
    var bot_right = bounds.getSouthWest();
    var hdif = Math.abs(top_left.lng() - bot_right.lng());
    var spacing = hdif/4;
    var position = {};

    var center = self.map.getCenter();
    var randomPoints1 = new google.maps.LatLng(center.lat(), center.lng()-spacing);
    var randomPoints2 = new google.maps.LatLng(center.lat(), center.lng()+spacing);

    self.getLoadPoint(randomPoints1, function(result) {
      position = result.routes[0].overview_path[0];
      self.pins.start.setPosition(position);
      self.pins.camera.setPosition(position);
      self.points.start = position;
      
      self.getLoadPoint(randomPoints2, function(result) {
        position = result.routes[0].overview_path[0];
        self.pins.end.setPosition(position);
        self.points.end = position;
        //changeHash();
      });
    });
  }
};